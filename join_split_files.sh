#!/bin/bash

cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat bootimg/01_dtbdump_MT6893.dtb.* 2>/dev/null >> bootimg/01_dtbdump_MT6893.dtb
rm -f bootimg/01_dtbdump_MT6893.dtb.* 2>/dev/null
cat system/system/app/VivoGallery/VivoGallery.apk.* 2>/dev/null >> system/system/app/VivoGallery/VivoGallery.apk
rm -f system/system/app/VivoGallery/VivoGallery.apk.* 2>/dev/null
cat system/system/app/AIService/AIService.apk.* 2>/dev/null >> system/system/app/AIService/AIService.apk
rm -f system/system/app/AIService/AIService.apk.* 2>/dev/null
cat system/system/app/VivoThemeRes/VivoThemeRes.apk.* 2>/dev/null >> system/system/app/VivoThemeRes/VivoThemeRes.apk
rm -f system/system/app/VivoThemeRes/VivoThemeRes.apk.* 2>/dev/null
cat system/system/app/VivoCamera/VivoCamera.apk.* 2>/dev/null >> system/system/app/VivoCamera/VivoCamera.apk
rm -f system/system/app/VivoCamera/VivoCamera.apk.* 2>/dev/null
cat system/system/app/VideoPlayer/VideoPlayer.apk.* 2>/dev/null >> system/system/app/VideoPlayer/VideoPlayer.apk
rm -f system/system/app/VideoPlayer/VideoPlayer.apk.* 2>/dev/null
cat system/system/app/HybridPlatform/HybridPlatform.apk.* 2>/dev/null >> system/system/app/HybridPlatform/HybridPlatform.apk
rm -f system/system/app/HybridPlatform/HybridPlatform.apk.* 2>/dev/null
cat system/system/app/BBKMusic/BBKMusic.apk.* 2>/dev/null >> system/system/app/BBKMusic/BBKMusic.apk
rm -f system/system/app/BBKMusic/BBKMusic.apk.* 2>/dev/null
cat system/system/custom/app/SmartOffice/lib/arm64/liblo-native-code.so.* 2>/dev/null >> system/system/custom/app/SmartOffice/lib/arm64/liblo-native-code.so
rm -f system/system/custom/app/SmartOffice/lib/arm64/liblo-native-code.so.* 2>/dev/null
cat system/system/custom/app/ihealth/ihealth.apk.* 2>/dev/null >> system/system/custom/app/ihealth/ihealth.apk
rm -f system/system/custom/app/ihealth/ihealth.apk.* 2>/dev/null
cat system/system/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/priv-app/Settings/Settings.apk
rm -f system/system/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system/system/priv-app/VivoBrowser/lib/arm64/libwebviewchromium_vivo.so.* 2>/dev/null >> system/system/priv-app/VivoBrowser/lib/arm64/libwebviewchromium_vivo.so
rm -f system/system/priv-app/VivoBrowser/lib/arm64/libwebviewchromium_vivo.so.* 2>/dev/null
cat system/system/priv-app/VivoBrowser/VivoBrowser.apk.* 2>/dev/null >> system/system/priv-app/VivoBrowser/VivoBrowser.apk
rm -f system/system/priv-app/VivoBrowser/VivoBrowser.apk.* 2>/dev/null
cat system/system/priv-app/AiAgent/AiAgent.apk.* 2>/dev/null >> system/system/priv-app/AiAgent/AiAgent.apk
rm -f system/system/priv-app/AiAgent/AiAgent.apk.* 2>/dev/null
cat system/system/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system/system/priv-app/SystemUI/SystemUI.apk
rm -f system/system/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system/system/priv-app/JoviIme/JoviIme.apk.* 2>/dev/null >> system/system/priv-app/JoviIme/JoviIme.apk
rm -f system/system/priv-app/JoviIme/JoviIme.apk.* 2>/dev/null
