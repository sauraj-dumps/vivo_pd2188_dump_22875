#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    io_enhance.sh \
    install-recovery.sh \
    init.vivo.fingerprint.sh \
    collect_connsys_dump.sh \
    zramsize_reconfig.sh \
    init.insmod.sh \
    init.vivo.nfc.sh \
    init.vivo.fingerprint_restart_counter.sh \
    init.vivo.crashdata.sh \

PRODUCT_PACKAGES += \
    fstab.emmc \
    factory_init.rc \
    init.project.rc \
    init.connectivity.rc \
    factory_init.project.rc \
    init_conninfra.rc \
    meta_init.connectivity.common.rc \
    multi_init.rc \
    init.mt6893.usb.rc \
    meta_init.modem.rc \
    meta_init.rc \
    init.factory.rc \
    init.modem.rc \
    factory_init.connectivity.rc \
    factory_init.connectivity.common.rc \
    init.sensor_2_0.rc \
    init.cgroup.rc \
    init.mt6893.rc \
    init.aee.rc \
    meta_init.connectivity.rc \
    init.ago.rc \
    init.connectivity.common.rc \
    meta_init.project.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.emmc:$(TARGET_COPY_OUT_RAMDISK)/fstab.emmc

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 31

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/vivo/PD2188/PD2188-vendor.mk)
