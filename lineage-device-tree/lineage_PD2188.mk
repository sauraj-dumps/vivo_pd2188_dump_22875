#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from PD2188 device
$(call inherit-product, device/vivo/PD2188/device.mk)

PRODUCT_DEVICE := PD2188
PRODUCT_NAME := lineage_PD2188
PRODUCT_BRAND := vivo
PRODUCT_MODEL := V2188A
PRODUCT_MANUFACTURER := vivo

PRODUCT_GMS_CLIENTID_BASE := android-vivo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_k6893v1_64-user 12 SP1A.210812.003 compiler11122242 release-keys"

BUILD_FINGERPRINT := vivo/PD2188/PD2188:12/SP1A.210812.003/compiler11122242:user/release-keys
